let imageList = { samples : [], latest  : [] },
  displayList = [];

const fs   = require('fs'),
  http     = require('http'),
  path     = require('path'),
  settings = require('./settings'),
  chokidar = require('chokidar'),
  nextImg = () => {
    const img = displayList.shift()

    console.log(`Display: ${img[0]} wait ${settings[img[1]].duration} ms`);

    if (!settings.showOnce || img[1] === 'samples') {
      displayList.push(img)
    }

    return JSON.stringify({
      url      : img[0],
      duration : settings[img[1]].duration
    });
  };

chokidar.watch(settings.samples.dir).on('add', (path, stats) => {
  console.log(`Added: ${path}`)
  imageList.samples = [[path, stats.ctime], ...imageList.samples].sort((a, b) => b[1] - a[1])
  displayList = [...displayList, [path, 'samples']]
})
chokidar.watch(settings.samples.dir).on('unlink', path => {
  console.log(`Removed: ${path}`)
  imageList.samples = imageList.samples.filter(item => item[0] !== path)
  displayList = displayList.filter(item => item[0] !== path)
})

chokidar.watch(settings.latest.dir).on('add', (path, stats) => {
  console.log(`Added: ${path}`)
  imageList.latest = [[path, stats.ctime], ...imageList.latest].sort((a, b) => b[1] - a[1])
  if (settings.showOnce) {
    displayList = [[path, 'latest'], ...displayList]
  } else {
    displayList = imageList.latest.map(img => [img[0], 'latest'])
  }
})
chokidar.watch(settings.latest.dir).on('unlink', path => {
  console.log(`Removed: ${path}`)
  imageList.latest = imageList.latest.filter(item => item[0] !== path)
  if (settings.showOnce) {
    displayList = displayList.filter(item => item[0] !== path)
  } else if (imageList.latest.length > 0) {
    displayList = imageList.latest.map(img => [img[0], 'latest'])
  } else {
    displayList = imageList.samples.map(img => [img[0], 'samples'])
  }
})

console.log(settings);

http.createServer((req, res) => {
  const serve = (data, options) => {
    res.statusCode = options.statusCode || 200
    res.setHeader('Content-Type', options.type || 'text/plain')
    res.end(data)
  },
  error = (e) => serve(e, { statusCode: 404 });

  if (req.url === '/next') {
    serve(nextImg(), { type: 'application/json' })
  } else if (req.url === '/') {
    fs.readFile('index.html', 'utf8', (e, data) => {
      if (e) error(e)
      else serve(data, { type: 'text/html' })
    });
  } else {
    fs.readFile(req.url.substr(1), (e, data) => {
      if (e) error(e)
      else if (data) {
        serve(data, { type: `image/${path.extname(req.url)}` })
      }
    });
  }
}).listen(settings.port, () => console.log(`listening on port ${settings.port}`));
